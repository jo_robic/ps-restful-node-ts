var should = require('should');
var sinon = require('sinon');

describe('Book Controller Tests', function(){

  // Post method
  describe('Post', function(){
    it('should not allow a empty title on post', function() {
      var Book = function(book){ this.save = function(){} };

      var req = {
        body: {
          author: 'Jonathan R.'
        }
      };

      var res = {
        status: sinon.spy(),
        send: sinon.spy()
      };

      var bookController = require('../release/api/controllers/bookController')(Book);
      bookController.post(req, res);

      res.status.calledWith(400).should.equal(true, 'Bad status ' + res.status.args[0][0]);
      res.send.calledWith('Title is required').should.equal(true);

    });
  });

});
