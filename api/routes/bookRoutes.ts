/// <reference path="../../typings/express/express.d.ts"/>
/// <reference path="../../typings/node/node.d.ts"/>

import express = require('express');


var routes = function(Book){

	var bookRouter = express.Router();
	var bookController = require('./../controllers/bookController')(Book);

	bookRouter.route('/')
		.post(bookController.post)
		.get(bookController.getAll);

	/* Middleware */
	bookRouter.use('/:bookId', function(req:any, res, next){
		Book.findById(req.params.bookId, function(err, book){
				if (err)
					res.status(500).send(err);
				else if (book) {
					req.book = book;
					next();
				}
				else {
					res.status(404).send('no book found!');
				}
		});
	});


	bookRouter.route('/:bookId')
		.get(bookController.get)
		.put(bookController.put)
		.patch(bookController.patch)
		.delete(bookController.delete);


	return bookRouter;

};

module.exports = routes;
