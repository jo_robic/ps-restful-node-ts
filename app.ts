/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>
/// <reference path="typings/mongoose/mongoose.d.ts"/>
/// <reference path="typings/body-parser/body-parser.d.ts"/>

import express = require('express');
import mongoose = require('mongoose');
import bodyParser = require('body-parser');

var db;

if (process.env.ENV == 'Test') {
	console.log('----- ENV TEST ------');
	db = mongoose.connect('mongodb://localhost/bookAPI_test');
}
else {
	console.log('----- ENV PROD ------');
	db = mongoose.connect('mongodb://localhost/bookAPI');
}

var Book = require('./api/models/bookModel');

var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/* Routes Conf */
var bookRouter = require('./api/routes/bookRoutes')(Book);
app.use('/api/books', bookRouter);





app.get('/', function(req, res){
	res.send('welcome to my API!');
});

app.listen(port, function(){
	console.log('Running on PORT: ' + port);
});


module.exports = app;
