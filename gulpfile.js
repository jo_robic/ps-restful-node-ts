/// <reference path="typings/gulp/gulp.d.ts"/>

var gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	nodemon = require('gulp-nodemon'),
	gulpMocha = require('gulp-mocha'),
	supertest = require('supertest'),
	env = require('gulp-env'),
	ts = require('gulp-typescript'),
	tsProject = ts.createProject('tsconfig.json')
	merge = require('merge2');

var nodemon_instance;


gulp.task('default', ['scripts', 'test', 'watch'], function(){
	env({vars: { ENV: '' }});
	nodemon_instance = nodemon({
		script: 'release/app.js',
		env: {
			PORT: 8000,
		},
		ignore: ['./node_modules/**']
	})
	.on('restart', function(){
		console.log('Restarting! ' + process.env.ENV);
	});
});


gulp.task('scripts', function(){
	var tsResult = tsProject.src()
		.pipe(plumber( ))
		.pipe(ts(tsProject));

	 return tsResult.js.pipe(gulp.dest('release'));
});


gulp.task('watch', function(){
	gulp.watch("./**/*.ts", ["scripts", "test", function(){
		env({vars: { ENV: '' }});
		nodemon_instance.emit('restart');
	}]);
	gulp.watch("./test/**/*.js", ["test"]);
});

gulp.task('test', function(){
	env({vars: { ENV: 'Test' }});

	gulp.src('./test/**/*.spec.js')
		.pipe(plumber({errorHandler: function(err){}}))
		.pipe(gulpMocha({
			reporter: 'nyan'
		}));

});
